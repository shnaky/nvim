call plug#begin('~/.vim/plugged')

Plug 'autozimu/LanguageClient-neovim', {
	\ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'junegunn/fzf'
Plug 'leafgarland/typescript-vim'
Plug 'calviken/vim-gdscript3'

Plug 'w0rp/ale'
Plug 'OmniSharp/omnisharp-vim'

call plug#end()

colors zenburn
set number
set completeopt-=preview

set tabstop=4
set shiftwidth=4
set clipboard+=unnamedplus

autocmd Filetype typescript setlocal ts=2 sw=2 sts=2 expandtab
autocmd Filetype javascript setlocal ts=2 sw=2 sts=2 expandtab
autocmd Filetype dart setlocal ts=2 sw=2 sts=2 expandtab
autocmd Filetype html setlocal ts=2 sw=2 sts=2
autocmd Filetype css setlocal ts=2 sw=2 sts=2
autocmd Filetype yaml setlocal ts=2 sw=2 sts=2
autocmd Filetype yml setlocal ts=2 sw=2 sts=2
autocmd Filetype json setlocal ts=2 sw=2 sts=2

" netrw
let g:netrw_list_hide= '.*\.meta$'

" Deoplete
let g:deoplete#enable_at_startup = 1

" Godot
autocmd FileType gdscript3
       \ call deoplete#custom#buffer_option('auto_complete', v:false)

" LanguageClient
set hidden

let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
	\ 'java': ['/usr/local/bin/jdtls'],
	\ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
	\ 'typescript': ['/usr/local/bin/javascript-typescript-stdio'],
	\ }

" Ale
let g:ale_linters = {
	\ 'cs': ['OmniSharp'],
	\ 'javascript': []
	\ }

" OmniSharp
let g:OmniSharp_server_use_mono = 1

" Disable Arrow keys in Escape mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable Arrow keys in Insert mode
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>
